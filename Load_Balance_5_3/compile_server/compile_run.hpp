#pragma once
/*************************************
 *适配用户需求，定制通信协议字段
 *正确的调用compile and run
 *形成唯一文件名
 ************************************/

#include "compiler.hpp"
#include "runner.hpp"
#include "../comm/log.hpp"
#include "../comm/util.hpp"

#include <signal.h>
#include <unistd.h>
#include <json/json.h>

namespace ns_compiler_and_run
{
    using namespace ns_compiler;
    using namespace ns_runner;
    using namespace ns_log;
    using namespace ns_util;

    class ComPileAndRun
    {
    public:
        static std::string CodeToDesc(int code)
        {

        }
        /************************************
         * 输入:
         * code:  用户提交的代码
         * input: 用户给自己提交的代码对应的输入，不做处理
         * cpu_limit: 时间要求
         * mem_limit: 空间要求
         *
         * 输出:
         * 必填：
         * status: 状态码
         * reason: 请求结果
         * 选填：
         * stdout: 我的程序运行完的结果
         * stderr: 我的程序运行完的错误结果
         *
         * 参数：
         * in_json: {"code": "#include...", "input": "","cpu_limit":1, "mem_limit":10240}
         * out_json: {"status":"0", "reason":"出错原因","stdout":"","stderr":"",}
         ***************************************/
        static void Start(const std::string &in_json, std::string *out_json)
        {
            // 用Json完成序列化工作，将结构化数据转换成为一个字符串
            Json::Value in_value;
            Json::Reader reader;
            reader.parse(in_json, in_value);

            std::string code = in_value["code"].asString();
            std::string input = in_value["input"].asString();
            int cpu_limit = in_value["cpu_limit"].asInt();
            int mem_limit = in_value["mem_limit"].asInt();

            int status_code = 0;
            Json::Value out_value;
            int run_result = 0;
            std::string file_name;

            if (code.size() == 0)
            {
                status_code = -1;//代码为空
                goto END;
                
            }
            // 形成的文件名只具有唯一性，无后缀
            // 毫秒级时间戳+原子性递增唯一性：来保证唯一性
            file_name = FileUtil::UniqFileName();

            // 形成临时Src文件
            if (!FileUtil::WriteFile(PathUtil::Src(file_name), code))
            {
                status_code = -2; // 未知错误
                goto END;
            }

            if (!Compiler::Compile(file_name))
            {

                status_code = -3; // 代码编译的时候发生错误
                goto END;
            }

            run_result = Runner::Run(file_name, cpu_limit, mem_limit);

            if (run_result < 0)
            {
                status_code = -2; // 未知错误
                goto END;
            }
            else if (run_result > 0)
            {
                // 程序运行崩溃
                status_code = run_result;
            }
            else
            {
                // 运行成功
                status_code = 0;
            }   
        END: 
            out_value["status"] = status_code;
            out_value["reason"] = CodeToDesc(status_code);
            if(status_code == 0)
            {
                //整个过程全部成功
                out_value["stdout"] = FileUtil::ReadFile(PathUtil::Stdout(file_name));
                out_value["stderr"] = FileUtil::ReadFile(PathUtil::Stderr(file_name));
            }

            Json::StyleWriter writer;
            *out_json = writer.writrt.write(out_value);
    }
};

/*
// 最后再处理差错问题
out_value["status"] = -1;
out_value["reason"] = "用户提交的代码为空";

out_value["status"] = -2; // 未知错误
out_value["reason"] = "代码发生未知错误";

// 编译失败
out_value["status"] = -3; // 代码编译的时候发生错误
out_value["reason"] = FileUtil::ReadFile(PathUtil::CompilerError(file_name));

out_value["status"] = -2; // 未知错误
out_value["reason"] = "代码发生未知错误";

out_value["status"] = code;              // 代码运行报错
out_value["reason"] = SignoToDesc(code); // 将信号转换成报错的原因
*/