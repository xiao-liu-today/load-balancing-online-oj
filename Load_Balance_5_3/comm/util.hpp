#pragma once
#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>

namespace ns_util
{
    class TimeUtil
    {
        public:
            static std::string GetTimeStamp()
            {
                struct timeval _time;
                gettimeofday(&_time, nullptr);
                /*int gettimeofday(struct timeval *restrict tv,
                        struct timezone *_Nullable restrict tz);
                    第一个参数为输出型参数，第二个参数为时区        
                */
                return std::to_string(_time.tv_sec);
                //返回的是秒数的字符串
            }
    };

    const std::string temp_path = "./temp/";
    class PathUtil
    {
        public: 
            static std::string AddSuffix(const std::string &file_name, const std::string &suffix)
            {
                std::string path_name = temp_path;
                path_name += file_name;
                path_name += suffix;
                return path_name;
            }
            //构建源文件路径+后缀的完整文件名
            static std::string Src(const std::string &file_name)
            {
                return AddSuffix(file_name, ".cpp");
            }

            //构建可执行文件路径+后缀的完整文件名
            static std::string Exe(const std::string &file_name)
            {
                return AddSuffix(file_name, ".exe");
            }
            //构建该程序对应的编译报错路径+后缀的完整文件名
            static std::string CompilerError(const std::string &file_name)
            {
                return AddSuffix(file_name, ".compile_error");
            }

            static std::string Stdin(const std::string &file_name)
            {
                return AddSuffix(file_name, ".stdin");
            }

            static std::string Stdout(const std::string &file_name)
            {
                return AddSuffix(file_name, ".stdout");
            }

            //构建该程序对应的标准错误路径+后缀的完整文件名
            static std::string Stderr(const std::string &file_name)
            {
                return AddSuffix(file_name, ".stderr");
            }
    };
    class FileUtil{
        public:
            static bool IsFileExists(const std::string &path_name){
                struct stat st;
                if(stat(path_name.c_str(), &st) == 0){
                    //获取成功,文件已经存在
                    return true;
                }
                return false;
            }

            static std::string UniqFileName()
            {
                //TODO
                return "";
            }

            static bool WriteFile(const std::string &target, const std::string &content)
            {
                //TODO
                return false;
            }

            static std::string ReadFile(const std::string &target/*可能需要其他参数*/)
            {
                return "";
            }
    };
}