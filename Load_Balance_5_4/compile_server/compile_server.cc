#include "compile_run.hpp"
using namespace ns_compiler_and_run;

// 编译服务随时可能被多人亲贵，必须保证传上来的code形成源文件名具有唯一性
// 不然多个用户之间会相互影响
int main()
{
    //提供的编译服务，打包形成一个网络服务
    //cpp-httplib

    /***************************************
     * in_json: {"code": "#include...", "input": "","cpu_limit":1, "mem_limit":10240}
     * out_json: {"status":"0", "reason":"出错原因","stdout":"","stderr":"",}
     * 通过http 让 client 上传一个json string
     ***************************************/

    //下面的工作充当客户端请求的json串
    std::string in_json;

    Json::Value in_value;
    //R"()" 在括号内的全部按照字符处理，不考虑特殊符号
    in_value["code"] = R"(#include<iostream>
    int main(){
        std::cout << "test1" << std::endl;
        while(1);
        return 0;
    })";

    in_value["input"] = "";
    in_value["cpu_limit"] = 1;
    in_value["mem_limit"] = 10240*3;

    Json::FastWriter writer;
    in_json = writer.write(in_value);

    std::cout << in_json << std::endl;
    std::string out_json;
    ComPileAndRun::Start(in_json, &out_json);
    std::cout << out_json << std::endl;
    
    return 0;
}