#pragma once
#include <iostream>
#include <unistd.h>

#include "../comm/util.hpp"
// 只负责代码编译
namespace ns_compiler
{
    class Compiler
    {
    public:
        Compiler()
        {
        }
        ~Compiler()
        {
        }
        // 返回值：编译成功？true:false
        // 输入参数：编译的文件名
        // file_name.cpp -> ./temp/file_name.cpp
        //./temp/file_name.cpp -> ./temp/file_name.exe
        static bool Compile(const std::string &file_name)
        {
            pid_t pid = fork();
            if (pid < 0)
                return false;
            else if (pid == 0)
            {
                int _stderr = open(PathUtil::Stderr(file_name).c_str(), O_CREAT| O_WRONLYy,0644);
                if(_stderr < 0){
                    exit(1);
                }
                //重定向标准错误到_stderr
                dup2(_stderr, 2);

                // 子进程：调用编译器，完成对代码的编译
                // gtt -o src -std=c++11
                execlp("g++", "-o", PathUtil::Exe(file_name).c_str(),
                       PathUtil::Src(file_name).c_str(), "-std=c++11", nullptr);
                exit(1);
            }
            else{
                waitpid(pid, nullptr, 0);
                //编译是否成功
                if(FileUtil::IsFileExists(PathUtil::Exe(file_name))){
                    return true;
                }
            }
            return false;
        }
    }
}