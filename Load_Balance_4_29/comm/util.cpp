#pragma once
#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
namespace ns_util
{
    const std::string temp_path = "./temp/";
    class PathUtil
    {
        public: 
            static std::string AddSuffix(const std::string &file_name, const std::string &suffix)
            {
                std::string path_name = temp_path;
                path_name += file_name;
                path_name += suffix;
                return path_name;
            }
            //构建源文件路径+后缀的完整文件名
            static std::string Src(const std::string &file_name)
            {
                return AddSuffix(file_name, ".cpp");
            }

            //构建可执行文件路径+后缀的完整文件名
            static std::string Exe(const std::string &file_name)
            {
                return AddSuffix(file_name, ".exe");
            }
            //构建该程序对应的标准错误路径+后缀的完整文件名
            static std::string Stderr(const std::string &file_name)
            {

            }
    };
    class FileUtil{
        public:
            static bool IsFileExists(const std::string &path_name){
                struct stat st;
                if(stat(path_name.c_str(), &st) == 0){
                    //获取成功,文件已经存在
                    return true;
                }
                return false;
            }
    }
}