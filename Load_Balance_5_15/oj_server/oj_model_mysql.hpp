#pragma once
// MySQL 版本
#include "../comm/util.hpp"
#include "../comm/log.hpp"
#include "include/mysql.h"

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <cassert>
#include <fstream>

// 根据题目list文件，加载所有的题目信息到内存中
// model: 主要用来和数据进行交互，对外提供访问数据的接口

namespace ns_model
{
    using namespace std;
    using namespace ns_log;
    using namespace ns_util;

    struct Question
    {
        std::string number; // 题目编号，唯一
        std::string title;  // 题目标题
        std::string star;   // 题目难度：简单 中等 困难
        int cpu_limit;      // 题目时间要求(s)
        int mem_limit;      // 题目空间要求(KB)
        std::string desc;   // 题目描述
        std::string header; // 题目预设给用户在线编辑器的代码
        std::string tail;   // 题目测试用例，需要和header拼接
    };

    const std::string oj_questions = "oj_questions";

    class Model
    {

    public:
        Model()
        {}

        bool QueryMySql(const std::string &sql, vector<Question> *out)
        {

        }

        bool GetAllQuestions(vector<Question> *out)
        {
            std::string sql = "select * from ";
            sql += oj_questions;
            return QueryMySql(sql, out);
        }

        bool GetOneQuestion(const std::string &number, Question *q)
        {
            bool res = false;
            std::string sql = "select * from ";
            sql += oj_questions;
            sql += " where number=";
            sql += number;
            vector<Question> result;
            if(QueryMySql(sql, &result))
            {
                if(result.size() == 1){
                    *q = result[0];
                    res = true;
                }
            }
            return res;
        }

        ~Model()
        {}
    };

} // namespace ns_model