#ifndef COMPILER_ONLINE

#include "header.cpp"

#endif

//TODO

void Test1()
{
    //通过定义临时对象，来完成方法的调用
    bool ret = Solution().isPalindrome(121);
    if(ret){
        std::cout << "通过用例1，测试121通过...OK!" << std::endl;
    }
    else{
        std::cout << "没有通过用例1，测试的值是：121" << std::endl;
    }
}

void Test2()
{
    //通过定义临时对象，来完成方法的调用
    bool ret = Solution().isPalindrome(-10);
    if(!ret){
        std::cout << "通过用例2，测试-10通过...OK!" << std::endl;
    }
    else{
        std::cout << "没有通过用例2，测试的值是：-10" << std::endl;
    }
}

int main()
{
    Test1();
    Test2();

    return 0;
}

// 当用户提交自己的代码的时候，oj不是只把上面的代码提交给conpile_and_run，而是加上测试用例和主函数
// 提交给后台编译：main函数+测试用例+用户代码