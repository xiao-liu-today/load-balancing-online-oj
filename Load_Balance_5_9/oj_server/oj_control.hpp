#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <mutex>
#include <cassert>

#include "../comm/util.hpp"
#include "../comm/log.hpp"
#include "../comm/httplib.h"

#include "oj_model.hpp"
#include "oj_view.hpp"

namespace ns_control
{
    using namespace std;
    using namespace ns_log;
    using namespace ns_util;
    using namespace ns_model;
    using namespace ns_view;

    // 提供服务的主机
    class Machine
    {
    public:
        std::string ip;  // 编译服务的ip
        int port;        // 编译服务的port
        uint64_t load;   // 编译服务的负载
        std::mutex *mtx; // mutex禁止拷贝，使用指针

    public:
        Machine() : ip(""), port(0), load(0), mtx(nullptr)
        {}

        ~Machine() {}

    public:
        // 提升主机负载
        void IncLoad()
        {
            if (mtx) mtx->lock();
            ++load;
            if (mtx) mtx->unlock();
        }
        // 减少主机负载
        void DecLoad()
        {
            if (mtx) mtx->lock();
            ++load;
            if (mtx) mtx->unlock();
        }

        // 获取主机负载,没有太大的意义，只是为了统一接口
        uint64_t Load()
        {
            uint64_t _load = 0;
            if (mtx) mtx->lock();
            _load = load;
            if (mtx) mtx->unlock();

            return _load;
        }
    };

    const std::string service_machine = "./conf/service_machine.conf";

    // 负载均衡模块
    class LoadBlance
    {
    private:
        // 可以给我们提供编译服务的所有的主机
        // 每一台主机都有自己的下标，充当当前主机的id
        std::vector<Machine> machines;
        // 所有在线的主机id
        std::vector<int> online;
        // 所有离线的主机id
        std::vector<int> offline;
        // 保证LoadBlance的数据安全
        std::mutex mtx;

    public:
        LoadBlance()
        {
            assert(LoadConf(service_machine));
            LOG(INFO) << "加载：" << service_machine << "成功！" << "\n";
        }

        ~LoadBlance()
        {}

    public:
        bool LoadConf(const std::string &machine_conf)
        {
            std::ifstream in(machine_conf);
            if(!in.is_open())
            {
                LOG(FATAL) << "加载：" << machine_conf << " 失败" << "\n";
                return false;
            }
            std::string line;
            while(std::getline(in, line))
            {
                std::vector<std::string> tokens;
                StringUtil::SplitString(line, &tokens, ":");
                if(tokens.size() != 2)
                {
                    LOG(WARNING) << " 切分 " << line << " 失败" << "\n";
                    continue;
                }
                Machine m;
                m.ip  = tokens[0];
                m.port = atoi(tokens[1].c_str());
                m.load = 0;
                m.mtx = new std::mutex();

                online.push_back(machines.size());
                machines.push_back(m);
            }
            in.close();
            return true;

        }

        //id: 输出型参数 m: 输出型参数
        bool SmartChoice(int *id, Machine **m)
        {
            // 1. 使用选择好的主机(更新该主机的负载)

            // 2. 可能离线主机 
            mtx.lock();
            //负载均衡算法
            //1. 随机数+hash
            //2. 轮询+hash
            int online_num = online.size();
            if (online_num == 0)
            {
                mtx.unlock();
                LOG(FATAL) << " 所有的后端编译主机已经离线, 请运维的同事尽快查看" << "\n";
                return false;
            }


            //通过遍历的方式，找到所有负载最小的机器
            *id = online[0];
            *m  = &machines[online[0]];//可能存在不满足的情况
            uint64_t min_load = machines[online[0]].Load();
            for (int i = 0; i < online_num; i++)
            {
                uint64_t curr_load = machines[online[i]].Load();
                if(min_load < curr_load)
                {
                    min_load = curr_load;
                    *id = online[i];
                    *m  = &machines[online[i]];
                }
            }
            mtx.unlock();
            return true;
        }

        void OfflineMachine(int which)
        {

        }

        void OnlineMachine()
        {


        }
    };

    // 这是核心业务逻辑的控制器
    class Control
    {
    private:
        Model model_;           // 提供后台数据
        View view_;             // 提供html渲染功能
        LoadBlance load_blance; // 核心负载均衡
    public:
        Control()
        {
        }
        ~Control()
        {
        }

    public:
        // 根据题目数据构建网页
        //  html: 输出型参数
        bool AllQuestions(string *html)
        {
            bool ret = true;
            vector<struct Question> all;
            if (model_.GetAllQuestions(&all))
            {
                sort(all.begin(), all.end(), [](const struct Question &q1, const struct Question &q2)
                     { return atoi(q1.number.c_str()) < atoi(q2.number.c_str()); });
                // 获取题目信息成功，将所有的题目数据构建成网页
                view_.AllExpandHtml(all, html);
            }
            else
            {
                *html = "获取题目失败, 形成题目列表失败...";
                ret = false;
            }
            return ret;
        }
        bool Question(const string &number, string *html)
        {
            bool ret = true;
            struct Question q;
            if (model_.GetOneQuestion(number, &q))
            {
                // 获取指定题目信息成功，将所有的题目数据构建成网页
                view_.OneExpandHtml(q, html);
            }
            else
            {
                *html = "指定题目: " + number + " 不存在...";
                ret = false;
            }
            return ret;
        }

        void Judge(const std::string &number, const std::string in_json, std::string *out_json)
        {
            // 1.in_json进行反序列化，得到题目id，得到用户提交源代码，input

            // 2. 重新拼接用户代码+测试用例代码，形成新的代码
            // 3. 选择负载最低的主机
            // 4. 然后发起http请求，得到结果
            // 5. 将结果赋值给out_json
        }
    };
}; // namespace ns_control