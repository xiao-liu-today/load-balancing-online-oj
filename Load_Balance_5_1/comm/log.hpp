#pragma once

#include <iostream>
#include <string>
#include "util.hpp"
namespace ns_log
{
    using namespace ns_util;
    //日志等级
    enum{
        INFO,
        DEBUG,  //调试日志
        WARNING,//出错，但不影响后续运行
        ERROR,  //用户当前请求错误，不能继续
        FATAL   //系统出现不能运行的错误
    };
    //LOG() << "message"
    inline std::ostream &Log(const std::string &level, const std::string &file_name, int line)
    {
        //日志等级
        std::string message = "[";
        message += level;
        message += "]";

        //报错文件名称
        message += "[";
        message += file_name;
        message += "]";

        //添加报错行
        message += "[";
        message += std::to_string(line);
        message += "]";

        //日志时间戳
        message += "[";
        message += TimeUtil::GetTimeStamp();//时间戳
        message += "]";

        std::cout << message;
        return std::cout;
    }

    //开放式日志(宏替换)
    #define LOG(level) Log(#level, __FILE__, __LINE__)

    
}