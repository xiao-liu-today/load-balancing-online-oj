#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "../comm/util.hpp"
#include "../comm/log.hpp"
#include "../comm/httplib.h"

#include "oj_model.hpp"

namespace ns_control
{
    using namespace std;
    using namespace ns_log;
    using namespace ns_util;
    using namespace ns_model;

    class Control
    {
    private:
        Model model_; // 提供后台数据

    public:
        Control()
        {
        }
        ~Control()
        {
        }

    public:
        
        // 根据题目数据构建网页
        //  html: 输出型参数
        bool AllQuestions(string *html)
        {
            bool ret = true;
            vector<struct Question> all;
            if (model_.GetAllQuestions(&all))
            {
                sort(all.begin(), all.end(), [](const struct Question &q1, const struct Question &q2){
                    return atoi(q1.number.c_str()) < atoi(q2.number.c_str());
                });
                // 获取题目信息成功，将所有的题目数据构建成网页
                
            }
            else
            {
                *html = "获取题目失败, 形成题目列表失败...";
                ret = false;
            }
            return ret;
        }
        bool Question(const string &number, string *html)
        {
            bool ret = true;
            struct Question q;
            if (model_.GetOneQuestion(number, &q))
            {
                // 获取指定题目信息成功，将所有的题目数据构建成网页

            }
            else
            {
                *html = "指定题目: " + number + " 不存在...";
                ret = false;
            }
            return ret;
        }
    };
}; // namespace ns_control